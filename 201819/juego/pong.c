#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

struct TObjeto{
        int x;
        int y;
};


/*void drawRaquet(struct TObjeto object, long int loc, char *map){
  for(int y = object.y; y< (object.y +900); y++  )
  for(int x = object.x; x<(object.x+100); x++){
  loc = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
  if(vinfo.bits_per_pixel == 32){
 *(map + loc) = 255;
 *(map + loc + 1) = 255;
 *(map + loc + 2) = 255;
 *(map + loc + 3) = 0;
 }else{
 int b = 255;
 int g = 255;
 int r = 255;
 unsigned short int tot = r<<11 | g<<5 | b;
 *((unsigned short int *)(map+loc)) = tot;
 }
 }
 }*/

int
main(int argc, char *argv[]){
        int fd = 0;
        long int screensize = 0;
        /*struct TObjeto jugador1;
        struct TObjeto jugador2;
        struct TObjeto pelota;*/
        int x = 0, y = 0;
        long int location = 0;
        char *fbp = 0;
        struct fb_var_screeninfo vinfo;
        struct fb_fix_screeninfo finfo;

        fd = open("/dev/fb0",O_RDWR);
        if(fd == -1){
                perror("NOT FOUND BRO");
                return EXIT_FAILURE;
        }
        //Se abre el fd
        if(ioctl(fd,FBIOGET_FSCREENINFO,&finfo)==-1){
                perror("ERROR READING FIXED INFORMATION");
                return EXIT_FAILURE;
        }
        if(ioctl(fd,FBIOGET_FSCREENINFO,&vinfo)==-1){
                perror("ERROR READING VARIABLE INFORMATION");
                return EXIT_FAILURE;
        }

        screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

        fbp = (char *) mmap(0,screensize,PROT_READ | PROT_WRITE, MAP_SHARED,fd,0);
        if((int)fbp == -1){
                perror("HOLA");
                exit(4);
        }
        x = 100;
        y = 100;

        for(y = 100; y<300; y++)
                for(x = 100; x < 300; x++){
                        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) +
                                (y+vinfo.yoffset) * finfo.line_length;
                        if(vinfo.bits_per_pixel == 32){
                                *(fbp+location) = 255;
                                *(fbp+location+1) = 255;
                                *(fbp+location+2) = 255;
                                *(fbp+location+3) = 0;
                        }else{
                                int b = 10;
                                int g = (x-100)/6;
                                int r = 31-(y-100)/16;
                                unsigned short int t = r<<11 | g<<5 | b;
                                *((unsigned short int*)(fbp + location)) = t;
                        }
                }
        munmap(fbp,screensize);
        close(fd);
        return EXIT_SUCCESS;
}

