#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <strings.h>
sig_atomic_t sgn = 0;
void gestor_sigint(int signal){
        sgn = 1;
}
int
main(int argc, char *argv[]){
        struct sigaction ramon;
        bzero(&ramon,sizeof(ramon));
        ramon.sa_handler = &gestor_sigint;
        sigaction(SIGINT,&ramon,NULL);
        int num;

        while(sgn == 0){
                sleep(1);
                if(sgn == 1){
                        scanf(" %i",&num);
                        printf("%i\n",num);
                }
        }

	return EXIT_SUCCESS;
}
